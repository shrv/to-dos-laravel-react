<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function todo(Request $request)
    {
        $todo_list          = Todo::all();
        $item_left          = Todo::where('status', 0)->get()->count();
        $count              = $todo_list->count();
        $all_completed      = $count != 0 ? ($item_left == 0 ? true : false) : false;

        $params = [
            'todo_list'     => $todo_list,
            'item_left'      => $item_left,
            'count'         => $count,
            'all_completed' => $all_completed
        ];
        return $params;
    }

    public function clear(Request $request)
    {
        if ($request->id != "all") {
            return json_encode(Todo::find($request->id)->delete());
        } else {
            $todo_list = Todo::where('status', true)->get();
            foreach ($todo_list as $todo) {
                if (!$todo->delete()) {
                    return json_encode(false);
                }
            }
            return json_encode(true);
        }
    }

    public function status(Request $request)
    {
        if (is_numeric($request->id)) {
            $todo = Todo::find($request->id);
            if ($todo->status == false) {
                $todo->status = true;
            } else {
                $todo->status = false;
            }
            return json_encode($todo->save());
        } else {
            $status = $request->id;
            return json_encode(Todo::where('status', $status)->update(['status' => !$status]));
        }
    }

    public function store(Request $request)
    {
        $is_modification = isset($request->id) ? true : false;
        $todo = $is_modification ? Todo::find($request->id) : new Todo;

        $todo->item = $request->item;

        if ($todo->save()) {
            return json_encode(true);
        }
        return json_encode(false);
    }
}
