import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import OutsideClick from "./OutsideClick";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todo_list: [],
            filter_List: [],
            item_left: false,
            count: false,
            all_completed: false,
            filter_type: "all",

            id: "",
            item: "",
            update: ""
        };
        this.store_method = this.store_method.bind(this);
        this.clean_state = this.clean_state.bind(this);
        this.update_field_switch = this.update_field_switch.bind(this);

        this.value_set_method = this.value_set_method.bind(this);
    }

    componentDidMount() {
        axios
            .get("/action/todo")
            .then(response => {
                this.setState({
                    todo_list: response.data.todo_list,
                    filter_List: response.data.todo_list,
                    item_left: response.data.item_left,
                    count: response.data.count,
                    all_completed: response.data.all_completed
                });
            })
            .catch(error => {
                console.log("data load failed " + error.response.data.errors);
            });
    }

    store_method(event) {
        event.preventDefault();

        const todo = {
            id: this.state.id,
            item: this.state.id != "" ? this.state.update : this.state.item
        };

        if (todo.item != "") {
            axios
                .post("/action/todo/store", todo)
                .then(response => {
                    this.componentDidMount();
                    this.clean_state();
                })
                .catch(error => {
                    console.log(
                        "data stored failed " + error.response.data.errors
                    );
                });
        }
    }

    clean_state() {
        this.setState({
            id: "",
            item: "",
            update: ""
        });
    }

    update_state_set(todo) {
        this.setState({
            id: todo.id,
            update: todo.item
        });
    }

    status_change(id) {
        axios
            .post("/action/todo/status", { id: id })
            .then(response => {
                this.componentDidMount();
            })
            .catch(error => {
                console.log(
                    "Status Change Error: " + error.response.data.errors
                );
            });
    }

    clear_completed(id) {
        axios
            .post("/action/todo/clear", { id: id })
            .then(response => {
                this.componentDidMount();
            })
            .catch(error => {
                console.log(
                    "Clear Completed Failed: " + error.response.data.errors
                );
            });
    }

    filter_set_method(type) {
        const { todo_list } = this.state;
        if (type == "all") {
            this.setState({
                filter_List: todo_list,
                filter_type: "all"
            });
        } else {
            let filtered_list = todo_list.filter(todo => {
                if (todo.status == type) {
                    return todo;
                }
            });
            this.setState({
                filter_List: filtered_list,
                filter_type: type ? "completed" : "active"
            });
        }
    }

    outer_click() {
        this.clean_state();
    }

    todo_list_method() {
        const { filter_List } = this.state;
        let element = filter_List.map(todo => (
            <div
                className="w-1/2 mx-auto flex shadow-lg border bg-white"
                key={todo.id}
            >
                <div className="w-1/12 flex justify-center items-center pl-5">
                    <img
                        className="cursor-pointer"
                        src={
                            todo.status == true
                                ? "/svg/check-circle.svg"
                                : "/svg/circle.svg"
                        }
                        onClick={this.status_change.bind(this, todo.id)}
                        title={
                            todo.status == true
                                ? "make it deselect..."
                                : "make it done..."
                        }
                    />
                </div>
                {this.update_field_switch(todo)}
            </div>
        ));
        return element;
    }

    update_field_switch(todo) {
        if (this.state.id != todo.id) {
            const element = (
                <React.Fragment>
                    <form
                        className="w-11/12 group flex justify-center items-center"
                        title="Double Click to enabled Input field"
                        onSubmit={this.store_method}
                    >
                        <input
                            type="text"
                            className={
                                todo.status == true
                                    ? "rounded-rt-lg line-through text-gray-500 focus:outline-none w-full p-5 text-3xl cursor-default"
                                    : "rounded-rt-lg focus:outline-none w-full p-5 text-3xl cursor-default"
                            }
                            value={todo.item}
                            readOnly
                            onDoubleClick={this.update_state_set.bind(
                                this,
                                todo
                            )}
                        />
                        <a
                            className="hidden group-hover:block mr-5 px-2 focus:outline-none"
                            onClick={this.clear_completed.bind(this, todo.id)}
                            title="delete from list"
                        >
                            <img src="/svg/close.svg" />
                        </a>
                    </form>
                </React.Fragment>
            );
            return element;
        } else {
            const element = (
                <React.Fragment>
                    <OutsideClick outside_check={this.outer_click.bind(this)}>
                        <form
                            className="group flex justify-center items-center"
                            title="Input field enabled to write"
                            onSubmit={this.store_method}
                        >
                            <input
                                type="text"
                                className={
                                    todo.status == true
                                        ? "rounded-rt-lg line-through text-gray-500 focus:outline-none w-full p-5 text-3xl"
                                        : "rounded-rt-lg focus:outline-none w-full p-5 text-3xl"
                                }
                                name="update"
                                value={this.state.update}
                                onChange={this.value_set_method}
                            />
                            <a
                                className="hidden group-hover:block mr-5 px-2 focus:outline-none"
                                onClick={this.clear_completed.bind(
                                    this,
                                    todo.id
                                )}
                            >
                                <img src="/svg/close.svg" />
                            </a>
                        </form>
                    </OutsideClick>
                </React.Fragment>
            );
            return element;
        }
    }

    footer_method() {
        const { item_left, count, filter_type } = this.state;
        let active =
            "border px-2 border-pink-200 focus:outline-none focus:border-pink-200 rounded-sm";
        let not_active =
            "border px-2 hover:border-pink-200 focus:outline-none focus:border-pink-200 rounded-sm";

        if (count) {
            let element = (
                <div className="w-1/2 mx-auto shadow-lg bg-white border">
                    <div className="flex justify-between">
                        <div className="text-center px-4 py-2 m-2">
                            {item_left == 1
                                ? item_left + " of " + count + " item left"
                                : item_left + " of " + count + " items left"}
                        </div>
                        <div className="text-center px-4 py-2 m-2">
                            <button
                                className={
                                    filter_type == "all" ? active : not_active
                                }
                                onClick={this.filter_set_method.bind(
                                    this,
                                    "all"
                                )}
                                title="all item of list"
                            >
                                All
                            </button>
                            <button
                                className={
                                    filter_type == "active"
                                        ? active + " mx-2"
                                        : not_active + " mx-2"
                                }
                                onClick={this.filter_set_method.bind(
                                    this,
                                    false
                                )}
                                title="all left item of list"
                            >
                                Active
                            </button>
                            <button
                                className={
                                    filter_type == "completed"
                                        ? active
                                        : not_active
                                }
                                onClick={this.filter_set_method.bind(
                                    this,
                                    true
                                )}
                                title="all completed item of list"
                            >
                                Completed
                            </button>
                        </div>
                        <div className="text-center px-4 py-2 m-2">
                            <button
                                className={
                                    item_left != count
                                        ? "focus:outline-none hover:underline"
                                        : "hidden"
                                }
                                onClick={this.clear_completed.bind(this, "all")}
                                title="remove all completed from list"
                            >
                                Clear Completed
                            </button>
                        </div>
                    </div>
                </div>
            );
            return element;
        }
    }

    render() {
        const { all_completed, count } = this.state;
        return (
            <React.Fragment>
                <div className="container mx-auto">
                    <div className="w-full">
                        <h1
                            className="font-normal text-center text-teal-200"
                            style={{ fontSize: 100 }}
                        >
                            to-dos
                        </h1>
                    </div>
                    <div className="w-1/2 mx-auto flex shadow-lg border">
                        <div className="w-1/12 bg-white flex justify-center items-center pl-5">
                            {count != 0 ? (
                                <img
                                    className="cursor-pointer"
                                    src={
                                        all_completed == true
                                            ? "/svg/check-circle.svg"
                                            : "/svg/circle.svg"
                                    }
                                    onClick={this.status_change.bind(
                                        this,
                                        all_completed
                                    )}
                                    title={
                                        all_completed == true
                                            ? "all deselect?"
                                            : "all done?"
                                    }
                                />
                            ) : null}
                        </div>
                        <form className="w-11/12" onSubmit={this.store_method}>
                            <input
                                type="text"
                                className="rounded-rt-lg focus:outline-none w-full italic p-5 text-3xl"
                                name="item"
                                value={this.state.item}
                                onChange={this.value_set_method}
                                placeholder="add to do ..."
                                autoComplete="off"
                            />
                            <button className="hidden" type="submit"></button>
                        </form>
                    </div>
                    {this.todo_list_method()}
                    {this.footer_method()}
                </div>
            </React.Fragment>
        );
    }

    value_set_method(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }
}

if (document.getElementById("app")) {
    ReactDOM.render(<App />, document.getElementById("app"));
}
